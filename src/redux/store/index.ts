/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { createStore } from "redux";
import rootReducer from "../reducers/index";


// DEFINES
const store = createStore( rootReducer );


// Let's export our redux store:
export default store;
