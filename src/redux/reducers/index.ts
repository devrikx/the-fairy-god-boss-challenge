/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { ADD_POST, UPDATE_POST } from "../constants/action-types";


// DEFINES
const initialState =
{
    posts: <any>[]
};


// Create a reducer:
const rootReducer = ( state = initialState, action: any ) =>
{
    switch( action.type )
    {
        case ADD_POST:
        {
            // Return a new state object that is a copy of the original state
            // plus the additions present in the payload:
            return { ...state, posts: [...state.posts, action.payload] };
        }break;

        case UPDATE_POST:
        {
            // Grab the id of the post we're updating from the payload which
            // is the new comment were updating the respective post with:
            let id = action.payload.parentId;

            // Next, grab a copy of the respective post:
            let post = state.posts.find( ( post: any ) => post.id === id );
            if( post && post.length )
            {
                post = post[0];
            }

            // Ensure we set the post's 'updated' property to the value
            // of the comment's created property:
            post.updated = action.payload.created;

            // Next, create a new post which is the old post plus the new comment
            // present in our payload:
            let updatedPost = { ...post, comments: [ ...post.comments, action.payload ] };

            // Use the map function to build a new array populated by all the
            // original posts other than the one whos id matches that of the
            // updated post we just created - replace that one with our new post:
            let updatedPosts = state.posts.map( ( post: any ) => ( post.id === id ) ? updatedPost : post );

            // Return a new state object that is a copy of the original state
            // plus the changes from our payload applied, holding to the redux
            // rule of immutable state:
            return { ...state, posts: updatedPosts };
        }break;

        default:
        {
            return state;
        }break;
    }
};


// Export it:
export default rootReducer;
