/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from "react";
import { updatePost } from "../../redux/actions/index";
import { connect } from "react-redux";
import { MessageItemType, InputValidationType } from "../../library";
import { TimestampHelper, FormValidation } from "../../helpers";
import { CreateFeedbackForm } from "../../components/CreateFeedbackForm";


// DEFINES
export type FeedbackFormContainerProps = {
    id: number;
    parentId: number;
    author: string;
    content: string;
    created: string;
}

interface StateFromProps {
    posts: Array<MessageItemType>
}

interface DispatchFromProps {
    updatePost: ( comment: any ) => any
}

const mapStateToProps = ( state: any ) =>
{
    return { posts: state.posts };
};

const mapDispatchToProps = ( dispatch: any ) =>
{
    return {
      updatePost: ( comment: any ) => dispatch( updatePost( comment ) )
    };
};


/**
 * Feedback Form Container component
 *
 * @since 0.1.0
 */
export class FeedbackFormContainer extends React.Component<FeedbackFormContainerProps, any>
{
    /**
     * @var { string } displayName Always set the display name
     *
     * @since 0.1.0
     */
    displayName = "FeedbackFormContainer";


    /**
     * We'd set propTypes here if this wasn't Typescript.
     * We'd set default  props here if we wanted them.
     */


    /**
     * Class constructor
     *
     * @param props
     *
     * @since 0.1.0
     */
    constructor( props?: any )
    {
        // Always call super()!
        super( props );

        this.state =
        {
            id: "",
            parentId: props.id,
            content: "",
            author: "",
            created: "",
            formValid: false,
            validations: {
                content: {
                    valid: false,
                    exists: false,
                    required: true,
                    label: "",
                    input: "",
                    validation: {
                        class: "",
                        message: ""
                    }
                },
                author: {
                    valid: false,
                    exists: false,
                    required: true,
                    label: "",
                    input: "",
                    validation: {
                        class: "",
                        message: ""
                    }
                }
            }
        };

        // Here we bind our handlers to an instance of 'this':
        this.handleReplyContentChange = this.handleReplyContentChange.bind( this );
        this.handleAuthorChange = this.handleAuthorChange.bind( this );
        this.handleSubmit = this.handleSubmit.bind( this );
    }


    /**
     * Makes use of a validation helper for implementing form validation
     *
     * @param { string } field The field validation is run for
     * @param { string } value A fields input, converted to a string
     */
    getValidation( field: string, value: string )
    {
        // This method uses our validation helper for implementing form validation
        let options = {
            required: false,
            constrain: false,
            exclude: false,
            regex: /[~`!@#$%\^&*()_+=\-\[\]\\';,/{}|\\":<>\?]/g,
            errorMessage:""
        },
        validator = new FormValidation(),
        formField: InputValidationType;

        switch( field )
        {
            case 'content':
            {
                // We have some helper functions to simplify required code
                // when implementing form field validation:
                options.required = this.state.validations.content.required;
                options.constrain = false;
                options.exclude = false;

                formField = validator.stageValidation( value, options );
            }break;

            case 'author':
            {
                // We have some helper functions to simplify required code
                // when implementing form field validation:
                options.required = this.state.validations.author.required;
                options.constrain = true;
                options.exclude = true;
                options.regex = /[~`$%\^*+=\[\]\;/{}|\\"<>]/g;
                options.errorMessage = "The title may only contain alphanumeric and the following special characters: [!,?,:, @, #, &, (,), -]";

                formField = validator.stageValidation( value, options );
            }break;
        }

        return formField;
    }


    /**
     * Handles state and validation for the message content form field
     *
     * @param { Event } event
     *
     * @return { void }
     */
    handleReplyContentChange( event: any )
    {
        // Get the validation state for this input:
        let contentValidation = this.getValidation( event.target.id, event.target.value ),
            { validations, formValid } = this.state;

        // Update the state:
        validations = { ...validations, content: contentValidation  };
        formValid = ( validations.content.valid && validations.author.valid );

        // Set the post contents by the input provided:
        this.setState( { [event.target.id]: event.target.value } );
        this.setState( { validations,formValid }  );
    }


    /**
     * Handles state and validation for the author form field
     *
     * @param { Event } event
     *
     * @return { void }
     */
    handleAuthorChange( event: any )
    {
        // Get the validation state for this input:
        let authorValidation = this.getValidation( event.target.id, event.target.value ),
            { validations, formValid } = this.state;

        // Update the state:
        validations = { ...validations, author: authorValidation  };
        formValid = ( validations.content.valid && validations.author.valid );

        // Set the author by the input provided:
        this.setState( { [event.target.id]: event.target.value } );
        this.setState( { validations, formValid }  );
    }


    /**
     * Handles procedures required upon submission of our post a comment form
     *
     * @param event
     */
    handleSubmit( event: any )
    {
        event.preventDefault();

        // Create an instance of the form validation helper:
        let validator = new FormValidation();

        // Get a timestamp ready, by preparing the seconds since epoch. We'll use
        // the number as the id, its localeString() as the created timestamp, as
        // well as the intial updated timestamp:
        let d = new Date();
        let seconds = Math.round(d.getTime() / 1000);

        // Create the id:
        const id = seconds;

        // Get the parent id:
        const parentId = this.state.parentId;

        // Set the created timestamp:
        let stamper = new TimestampHelper();
        const created = stamper.getTimestamp( ( seconds * 1000 ) );

        console.log( "Timestamp: " + created );

        // Pull the user-contributed values:
        let { content, author, validations, formValid } = this.state;

        // Reset the form:
        event.target.reset();

        // Add the comment to the post:
        ( this.props as any ).updatePost( { id, parentId, author, content, created } );

        // Clear the state of this component for a subsequent run-through:
        validations.content = validator.reset( validations.content.required );
        validations.author = validator.reset( validations.author.required );

        formValid = ( validations.content.valid && validations.author.valid );

        this.setState( { content: "", author: "", validations: validations, formValid: false } );
    }


    /**
     * Container components pass rendering responsibilities off to
     * presentational components
     *
     * @param { void }
     *
     * @since 0.1.0
     */
    render()
    {
        // Move our state properies to variables for cleaner manipulation:
        const { content, author, validations, formValid } = this.state;

        return (
            <CreateFeedbackForm
             content={content}
             contentValidation={validations.content}
             author={author}
             authorValidation={validations.author}
             submitState={!formValid}
             handleAuthorChange={this.handleAuthorChange}
             handleReplyContentChange={this.handleReplyContentChange}
             handleSubmit={this.handleSubmit}
            />
        );
    }
}


// Here we wrap this FeedbackFormContainer with a Connect container,
// creating the FeedbackForm Container:
export default connect<StateFromProps, DispatchFromProps>(
    mapStateToProps,
    mapDispatchToProps
)( FeedbackFormContainer );
