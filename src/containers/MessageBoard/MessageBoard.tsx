/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from "react";
import { connect } from "react-redux";
import { MessageItemType } from "../../library";
import { MessageBoardList } from "../../components/MessageBoardList";


// DEFINES
export interface MessageBoardProps {
    posts: Array<MessageItemType>;
};

interface StateFromProps {
    posts: Array<MessageItemType>
}

const mapStateToProps = ( state: any ) =>
{
    return { posts: state.posts };
};


/**
 * Message board container component
 *
 * @since 0.1.0
 */
export class MessageBoardContainer extends React.Component<MessageBoardProps, any>
{
    /**
     * @var { string } displayName Always set the display name
     *
     * @since 0.1.0
     */
    displayName = "MessageBoardContainer";


    /**
     * We'd set propTypes here if this wasn't Typescript.
     * We'd set default  props here if we wanted them.
     */


    /**
     * Constructor
     *
     * @param props
     *
     * @since 0.1.0
     */
    constructor( props?: MessageBoardProps )
    {
        super( props );
    }


    /**
     * Container components pass rendering responsibilities off to
     * presentational components
     *
     * @param { void }
     *
     * @since 0.1.0
     */
    render()
    {
        return (
            <MessageBoardList
              posts={this.props.posts}
            />
        );
    }
}


// Here we wrap the MessageBoardCotnainer with a Connect container,
// creating the MessageBoard Container:
export default connect<StateFromProps>(
    mapStateToProps
)( MessageBoardContainer );
