import MessageBoard from "./MessageBoard";

/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


 // INCLUDES
 export {
    default,
    MessageBoardContainer,
    MessageBoardProps
} from "./MessageBoard";


 // DEFINES