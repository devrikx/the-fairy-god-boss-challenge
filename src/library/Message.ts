/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


 // INCLUDES
 import { FeedbackItemType } from "./Feedback";


 // DEFINES
 export type MessageItemType = {
    id: number;
    title: string;
    content: string;
    excerpt: string;
    author: string;
    created: string;
    updated: string;
    comments: FeedbackItemType[];
};