/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { CreateFeedbackFormProps, CreateFeedbackForm } from './';


// Basic CreateFeedbackForm Test:
it
(
    'CreateFeedbackForm renders correctly 1 level deep',
    () =>
    {
        let feedbackFormStuffs: CreateFeedbackFormProps =
        {
            author: "",
            content: "",
            contentValidation: {
                valid: false,
                exists: false,
                required: true,
                input: "is_invalid",
                label: "text-danger",
                validation: {
                    class: "invalid-feedback",
                    message: "This is a sample validation feedback message"
                }
            },
            authorValidation: {
                valid: true,
                exists: true,
                required: true,
                input: "is_valid",
                label: "text-success",
                validation: {
                    class: "valid-feedback",
                    message: "This is a sample validation feedback message"
                }
            },
            submitState: false,
            handleAuthorChange: () => {},
            handleReplyContentChange: () => {},
            handleSubmit: () => {}
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <CreateFeedbackForm
             author={feedbackFormStuffs.author}
             content={feedbackFormStuffs.content}
             authorValidation={feedbackFormStuffs.authorValidation}
             contentValidation={feedbackFormStuffs.contentValidation}
             submitState={feedbackFormStuffs.submitState}
             handleAuthorChange={feedbackFormStuffs.handleAuthorChange}
             handleReplyContentChange={feedbackFormStuffs.handleReplyContentChange}
             handleSubmit={feedbackFormStuffs.handleSubmit}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
