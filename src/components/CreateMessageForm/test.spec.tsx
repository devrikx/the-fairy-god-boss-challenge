/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { CreateMessageFormProps, CreateMessageForm } from './';


// Basic CreateMessageForm Test:
it
(
    'CreateMessageForm renders correctly 1 level deep',
    () =>
    {
        let messageFormStuffs: CreateMessageFormProps =
        {
            title: "",
            author: "",
            content: "",
            titleValidation: {
                valid: false,
                exists: false,
                required: true,
                input: "is_invalid",
                label: "text-danger",
                validation: {
                    class: "invalid-feedback",
                    message: "This is a sample validation feedback message"
                }
            },
            contentValidation: {
                valid: false,
                exists: false,
                required: true,
                input: "is_invalid",
                label: "text-danger",
                validation: {
                    class: "invalid-feedback",
                    message: "This is a sample validation feedback message"
                }
            },
            authorValidation: {
                valid: true,
                exists: true,
                required: true,
                input: "is_valid",
                label: "text-success",
                validation: {
                    class: "valid-feedback",
                    message: "This is a sample validation feedback message"
                }
            },
            submitState: false,
            handleTitleChange: () => {},
            handleAuthorChange: () => {},
            handlePostContentChange: () => {},
            handleSubmit: () => {}
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <CreateMessageForm
             title={messageFormStuffs.title}
             author={messageFormStuffs.author}
             content={messageFormStuffs.content}
             titleValidation={messageFormStuffs.titleValidation}
             contentValidation={messageFormStuffs.contentValidation}
             authorValidation={messageFormStuffs.authorValidation}
             submitState={messageFormStuffs.submitState}
             handleTitleChange={messageFormStuffs.handleTitleChange}
             handleAuthorChange={messageFormStuffs.handleAuthorChange}
             handlePostContentChange={messageFormStuffs.handlePostContentChange}
             handleSubmit={messageFormStuffs.handleSubmit}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
