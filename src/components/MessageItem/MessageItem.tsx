/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { NavLink } from "react-router-dom";
import { MessageItemType } from "../../library";
import { FeedbackList } from "../FeedbackList";
import FeedbackForm from '../../containers/FeedbackForm';


// DEFINES
export type MessageItemProps = {
    id: number
    post: MessageItemType;
};


/**
 * Message presentational component
 *
 * @param { MessageItemProps } props
 *
 * @since 0.1.0
 */
export const MessageItem: React.SFC<MessageItemProps> = ( props ) =>
{
    // Here we render the component:
    return(
        <div className={styles.messageItemContainer}>

            <div className={styles.focusContent}>

                <div className={styles.messageContentContainer}>
                    <div className={styles.postHeaderContainer}>
                        <div className={styles.postHeaderContainerTitle}>
                            <h1><span className={styles.titleCapture}>POST</span>: {props.post.title}</h1>
                        </div>
                        <div className={styles.postHeaderContainerDetails}>
                            <div className={styles.authorDetail}>
                                By: <span className={styles.fieldHighlight}>{props.post.author}</span>
                            </div>
                            <div className={styles.timestampDetail}>
                                On: <span className={styles.fieldHighlight}>{props.post.created}</span>
                            </div>
                        </div>
                    </div>
                    <div className={styles.postBodyContainer}>
                        <div className={styles.postBodyContent}>
                            <p>{props.post.content}</p>
                        </div>
                    </div>
                </div>

                <hr className={styles.purpleDividerTransition + " " + styles.fullWidth} />

                <div className={styles.messageCommentContainer}>
                    <span className={styles.sectionTitle}>Responses:</span>

                    <FeedbackList
                     post={props.post}
                    />

                    <hr className={styles.purpleDivider + " " + styles.fullWidth} />

                    <FeedbackForm id={props.id} />
                </div>

                <hr className={styles.purpleDivider + " " + styles.verticalButtonDivider} />

                <NavLink to="/">
                    <button type="button" className={"btn btn-outline-primary " + styles.backToBoardButton}>Back to Posts</button>
                </NavLink>
            </div>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
MessageItem.displayName = "MessageItem"


/**
 * We'd set the propTypes here if this wasn't Typescript.
 */


/**
 * @var { string } defaultProps An excellent facility for testing layout!
 *
 * @since 0.1.0
 */
MessageItem.defaultProps = {
    id: 1,
    post: {
        id: 1,
        title: "Test Post",
        author: "Richard B Winters",
        excerpt: "",
        content: "This is a test post for testing purposes...DUH!",
        created: "12/06/18 2:28 PM",
        updated: "12/06/18 2:29 PM",
        comments: [
            { id: 0, parentId: 0,  author: "Richard", content: "This is a funny post!", created: "09/06/18 @ 03:22 PM" },
            { id: 1, parentId: 0, author: "Paul", content: "Great post!", created: "09/06/18 @ 03:28 PM" }
        ]
    }
};
