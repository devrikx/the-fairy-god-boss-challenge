export const messageListContainer: string;
export const focusHeader: string;
export const titleCapture: string;
export const focusContent: string;
export const messageList: string;
export const noMessages: string;
export const messageContentContainer: string;
export const createPostButton: string;
