/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { MessageBoardListProps, MessageBoardList } from './';


// Basic MessageBoardList Test (empty):
it
(
    'MessageBoardList renders correctly with an empty posts array',
    () =>
    {
        let messageBoardList: MessageBoardListProps =
        {
            posts: []
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <MessageBoardList
              posts={messageBoardList.posts}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);


// Basic MessageBoardList Test (1 post):
it
(
    'MessageBoardList renders correctly with a single post in the posts array',
    () =>
    {
        let messageBoardList: MessageBoardListProps =
        {
            posts: [{
                id: 1,
                title: "Test Post",
                author: "Richard B Winters",
                excerpt: "",
                content: "This is a test post for testing purposes...DUH!",
                created: "12/06/18 2:28 PM",
                updated: "12/06/18 2:29 PM",
                comments: [
                    { id: 0, parentId: 0,  author: "Richard", content: "This is a funny post!", created: "09/06/18 @ 03:22 PM" },
                    { id: 1, parentId: 0, author: "Paul", content: "Great post!", created: "09/06/18 @ 03:28 PM" }
                ]
            }]
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render(
            <MessageBoardList
              posts={messageBoardList.posts}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
